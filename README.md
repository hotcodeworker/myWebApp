# Python 설치하기

```
$ sudo apt install python3.6

$ python3 --version
Python 3.6.1
```

# virtualenv 설치하기
home 디렉토리 아래 djangogirls라는 디렉토리를 새로 만들어 사용하도록 할게요.
```
$ mkdir djangogirls
$ cd djangogirls
```

이제 myenv 라는 이름의 가상환경을 만들어 볼게요
```
$ python3 -m venv myvenv
```
이렇게 했을 때 오류가 난다면 다음과 같이 virtualenv를 설치합니다.

## 가상환경 설치 
추가로 또 아래와 같이 설치가 필요할 수도 있어요.
```
$ sudo apt install python3-venv
```
리눅스와 맥에서 virtualenv를 생성하려면 간단하게 python3 -m venv myvenv를 입력하면 됩니다.
여기서 myvenv 는 가상환경의 이름이에요. 이름은 마음대로 정할 수 있지만, 소문자여야 하고 공백은 없어야 해요. 이름은 짧게 만드는 게 좋아요. 자주 입력해야 하니까요.(Referral:[https://tutorial.djangogirls.org/ko/installation/])

# 가상환경 사용하기

아래 명령으로 가상환경을 실행하세요.
```
$ source myvenv/bin/activate
```

가끔씩 source가 사용할 수 없을 수도 있습니다. 이 경우에는 아래와 같이 입력하세요.
```
$ . myvenv/bin/activate
```


# 장고 설치하기 (가상환경에서)

## pip 업데이트 
그전에 장고를 설치하는 데 필요한 pip이 최신 버전인지 확인합니다. 
```
(myvenv) ~$ python3 -m pip install --upgrade pip
```

## django 설치 
그런 다음 pip install django~=1.11.0(Django를 설치하려면 물결표 뒤에 등호 :~=)를 입력해 장고를 설치하세요.
```
(myvenv) ~$ pip install django~=1.11.0
Collecting django~=1.11.0
  Downloading Django-1.11.3-py2.py3-none-any.whl (6.8MB)
Installing collected packages: django
Successfully installed django-1.11.3
```

# 코드 에디터 설치하기
대부분의 파이썬 프로그래머는 PyCharm과 같이 복잡하지만 강력한 IDE(통합개발환경: Integrated Development Environments)를 이용합니다. 하지만 초보자에게 적합하지 않을 수 있어요.간단하지만 강력한 기능을 갖춘 에디터 프로그램을 아래 추천합니다.

##  Gedit

## Sublime Text 3

## Atom

# GitHub 계정 만들기

# PythonAnywhere 계정 만들기
Referral: [www.pythonanywhere.com]

사용자 이름을 정할 때 블로그 주소의 일부가 되는 것을 기억하세요. 예를 들어 사용자이름이 yourusername이면 URL은 yourusername.pythonanywhere.com이 된답니다.

# 나의 첫 번째 장고 프로젝트!
첫 단계는 장고 프로젝트를 시작하는 거예요. 다시 말해 장고의 기본 골격을 만들어주는 스크립트를 실행할 거예요. 이 디렉토리와 파일 묶음은 나중에 사용할 것입니다.

장고에서는 디렉토리와 파일명이 매우 중요하답니다. 파일명을 마음대로 변경해서도 안되고 다른 곳으로 옮겨도 안됩니다. 장고는 중요한 것들을 찾을 수 있게 특정한 구조를 유지해야 합니다.
```
$ django-admin.py startproject mysite
```

django-admin.py은 스크립트로 디렉토리와 파일들을 생성합니다. 스크립트 실행 후에는 아래와 같이 새로 만들어진 디렉토리 구조를 볼 수 있을 거예요.
```
djangogirls
├───manage.py
└───mysite
        settings.py
        urls.py
        wsgi.py
        __init__.py
```
- manage.py는 스크립트인데, 사이트 관리를 도와주는 역할을 합니다. 이 스크립트로 다른 설치 작업 없이, 컴퓨터에- 서 웹 서버를 시작할 수 있습니다.

- settings.py는 웹사이트 설정이 있는 파일입니다.

- 앞에 우편배달부는 어느 곳으로 편지를 배달해야 하는지 판단해야 한다고 말했던 것을 기억하고 있나요? urls.py파일은 urlresolver가 사용하는 패턴 목록을 포함하고 있습니다.

## 설정 변경
Settings.py에서 TIME_ZONE있는 줄을 찾으세요. 그리고 이를 해당 시간대로 변경하세요.
```
TIME_ZONE = 'Asia/Seoul'
```

다음으로 정적파일 경로를 추가할 거에요.
Settings.py에서 파일의 끝(end)으로 내려가서, STATIC_URL항목 바로 아래에 STATIC_ROOT을 추가하세요.
```
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
```

DEBUG가True이고 ALLOWED_HOSTS가 비어 있으면, 호스트는 ['localhost', '127.0.0.1', '[::1]']에 대해서 유효합니다. 애플리케이션을 배포할 때 PythonAnywhere의 호스트 이름과 일치하지 않으므로 다음 설정을 아래와 같이 변경해줘야 합니다.
```
ALLOWED_HOSTS = ['127.0.0.1', '.pythonanywhere.com']
```


## 데이터베이스 설정하기
사이트 내 데이터를 저장하기 위한 많은 다양한 데이터베이스 소프트웨어들이 있습니다. 그중에서 우리는 sqlite3을 사용할 거예요.

이미 mysite/settings.py파일 안에 설치가 되어있어요.
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
```
데이터베이스를 생성하기 위해서 콘솔 창에서 아래 코드를 실행하세요. : python manage.py migrate 를 실행하세요.
```
(myvenv) ~/djangogirls$ python manage.py migrate
Operations to perform:
  Apply all migrations: auth, admin, contenttypes, sessions
Running migrations:
  Rendering model states... DONE
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying sessions.0001_initial... OK
```

이제 웹 서버를 시작해 웹 사이트가 잘 작동하는지 확인해봐요!
```
(myvenv) ~/djangogirls$ python manage.py runserver
```

# 어플리케이션 만들기
```
(myvenv) ~/djangogirls$ python manage.py startapp blog
```

이제 blog디렉터리가 생성되고 그 안에 여러 파일도 같이 들어있는 것을 알 수 있어요.
```
 djangogirls
    ├── mysite
    |       __init__.py
    |       settings.py
    |       urls.py
    |       wsgi.py
    ├── manage.py
    └── blog
        ├── migrations
        |       __init__.py
        ├── __init__.py
        ├── admin.py
        ├── models.py
        ├── tests.py
        └── views.py
```

## Django 모델
- models.CharField - 글자 수가 제한된 텍스트를 정의할 때 사용합니다. 글 제목같이 짧은 문자열 정보를 저장할 때 사용합니다.
- models.TextField - 글자 수에 제한이 없는 긴 텍스트를 위한 속성입니다. 블로그 콘텐츠를 담기 좋겠죠?
- models.DateTimeField - 날짜와 시간을 의미합니다.
- models.ForeignKey - 다른 모델에 대한 링크를 의미합니다.

추가로,  모델의 필드와 정의하는 방법에 궁금하다면 장고 공식 문서를 꼭 읽어보길 바랍니다.
Referral: [ https://docs.djangoproject.com/en/1.11/ref/models/fields/#field-types]


## 데이터베이스에 모델을 위한 테이블 만들기
이제 데이터베이스에 우리의 새 모델, Post 모델을 추가할 거에요. 먼저 우리는 장고 모델에 (우리가 방금 만든!) 몇 가지 변화가 생겼다는 걸 알게 해줘야 합니다. 

python manage.py makemigrations blog 를 입력해 보세요. 아마도 화면에 이렇게 보이겠죠?
```
(myvenv) ~/djangogirls$ python manage.py makemigrations blog
Migrations for 'blog':
  blog/migrations/0001_initial.py:
  - Create model Post
```
장고는 데이터베이스에 지금 반영할 수 있도록 마이그레이션 파일(migration file)이라는 것을 준비해 두었답니다.


이제 python manage.py migrate blog 명령을 실행해, 실제 데이터베이스에 모델 추가를 반영하겠습니다.
```
(myvenv) ~/djangogirls$ python manage.py migrate blog
Operations to perform:
  Apply all migrations: blog
Running migrations:
  Rendering model states... DONE
  Applying blog.0001_initial... OK
```
드디어 글 모델이 데이터베이스에 저장되었습니다. 

# 장고 관리자
방금 막 모델링 한 글들을 장고 관리자에서 추가하거나 수정, 삭제할 수 있어요.
blog/admin.py 파일을 열어서 내용을 다음과 같이 바꾸세요.

앞에서 정의했던 Post모델을 가져오고(import) 있어요. 관리자 페이지에서 만든 모델을 보려면 blog/admin.py 파일파일에서 admin.site.register(Post)로 모델을 등록해야 해요.

이제 Post모델을 볼까요? 웹 서버를 실행하려면 콘솔 창에서 python manage.py runserver를 실행하는걸 잊지 마세요. 브라우저를 열고 주소창에 http://127.0.0.1:8000/admin/ 을 입력하면 아래와 같은 로그인 페이지를 볼 수 있어요.

로그인 페이지에서 로그인하기 위해서는, 모든 권한을 가지는 슈퍼 사용자(superuser)를 생성해야 해요. 커맨드라인으로 돌아가서 python manage.py createsuperuser을 입력하고 엔터를 누르세요.
```
(myvenv) ~/djangogirls$ python manage.py createsuperuser
Username: admin
Email address: admin@admin.com
Password:
Password (again):
Superuser created successfully.
```
장고 관리자에 대해서 좀 더 알고 싶다면 장고 공식 문서를 읽어보세요.
Referral: [https://docs.djangoproject.com/en/1.8/ref/contrib/admin/]


# 정규표현식(Regex)

URL패턴 만드는 방법이 궁금하다면, 다음 표기법을 확인하세요. 이 중 몇 가지 규칙만 사용할 거에요.

^ : 문자열이 시작할 떄
$ : 문자열이 끝날 때
\d : 숫자
: 바로 앞에 나오는 항목이 계속 나올 때
() : 패턴의 부분을 저장할 때

이외에도 문자열을 이용해 url을 만들 수 있어요.

예를 들면, http://www.mysite.com/post/12345/라는 사이트가 있다고 합시다. 여기에서 12345는 글 번호를 의미합니다.

뷰마다 모든 글 번호을 일일이 매기는 것은 정말 힘들죠. 정규표현식으로 url패턴을 만들어 숫자값과 매칭되게 할 수 있어요. 이렇게 말이죠. ^post/(\d+)/$. 어떤 뜻인지 하나씩 살펴봅시다.

^post/ : url이(오른쪽부터) post/로 시작합니다.
(\d+) : 숫자(한 개 이상)가 있습니다. 이 숫자로 조회하고 싶은 게시글을 찾을 수 있어요.
/ : /뒤에 문자가 있습니다.
$ : url 마지막이 /로 끝납니다.

# 나의 첫 번째 Django url
파이썬에서 정규 표현식을 작성할 때는 항상 문자열 앞에 r을 붙입니다.

mysite/urls.py 파일 수정합니다.

그리고 blog/urls.py 생성하기

# 장고 뷰 만들기
blog/view.py 수정

# 첫 번째 템플릿
템플릿은 blog/templates/blog디렉토리에 저장됩니다.
하위 디렉터리인 templates을 생성하세요. 그리고 template디렉토리 내 blog라는 하위 디렉토리를 생성하세요.
```
blog
└───templates
 └───blog
```

그리고 blog/templates/blog디렉토리 안에 post_list.html이라는 새 파일(현재는 빈 파일)을 만드세요.
- <em>text</em> - 텍스트 기울기 (Italic)
- <div></div> - 페이지 섹션

# 장고 ORM과 쿼리셋(QuerySets)
Rererral: [https://tutorial.djangogirls.org/ko/django_orm/]

## 쿼리셋이란 무엇인가요?
쿼리셋(QuerySet)은 전달받은 모델의 객체 목록입니다. 쿼리셋은 데이터베이스로부터 데이터를 읽고, 필터를 걸거나 정렬을 할 수 있습니다.

## 장고 쉘(shell)
```
(myvenv) ~/djangogirls$ python manage.py shell
(InteractiveConsole)
>>>
```


# 템플릿 동적 데이터
Post모델은 post_list파일에, post_list모델은 views.py파일에 있어요. 그리고 앞으로 템플릿도 추가해야 합니다.
콘텐츠(데이터베이스 안에 저장되어 있는 모델)를 가져와 템플릿에 넣어 보여주는 것을 해볼 거에요.


# CSS - 예쁘게 만들기
이제 눈에 보기 좋게 예쁘게 만들어 볼 시간이에요.

## CSS는 무엇인가요?
CSS(Cascading Style Sheets)는 HTML와 같이 마크업언어(Markup Language)로 작성된 웹사이트의 외관을 꾸미기 위해 사용되는 언어입니다.
아무것도 없는 상태에서 시작하기 어렵겠죠? 개발자들이 만든 오픈 소스 코드를 사용해 만들어 볼 거에요.

## 부트스트랩을 사용
부트스트랩(Bootstrap)은 유명한 HTML과 CSS프레임워크로 예쁜 웹사이트를 만들 수 있습니다.
Referral: [https://getbootstrap.com/]


## 부트스트랩 설치
.html파일 내 <head>에 하기 링크를 넣어야 합니다.
```
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
```

## 정적 파일
정적 파일은 CSS와 이미지 파일에 해당합니다. 이 컨텐츠는 요청 내용에 따라 바뀌는 것이 아니기 때문에 모든 사용자들이 동일한 내용을 볼 수 있어요.

서버에서 collectstatic를 실행할 때 처럼, 장고는 "admin"앱에서 정적 파일을 어디서 찾아야하는지 이미 알고 있어요. 이제 "blog"앱에 정적파일을 추가하면 됩니다.

"blog"앱 안에 static라는 새 폴더를 만드세요.
```
djangogirls
    ├── blog
    │   ├── migrations
    │   ├── static
    │   └── templates
    └── mysite
```
장고는 app 폴더 안에 있는 static폴더를 자동으로 찾을 수 있어요. 이 컨텐츠를 정적 파일로 사용하게 되는 것입니다.

## 나의 첫 번째 CSS 파일
```
    djangogirls
    └─── blog
         └─── static
              └─── css
                   └─── blog.css
```

CSS 파일에서는 HTML 파일에 있는 각 요소들에 스타일을 정의할 수 있어요. 요소를 식별하는 첫 번째 방법은 요소 이름을 사용하는 것입니다. HTML 섹션에서 태그로 기억할 수 있습니다. a,h1,body와 같은 것은 모두 요소 이름의 예입니다. 또 class 속성이나 id 속성에 의해 요소를 식별합니다. 클래스와 ID는 요소에 직접 부여한 이름이에요. 

무료 온라인 Codecademy HTML & CSS 코스를 수강해보는 것을 추천합니다. CSS로 웹 사이트를 더 아름답게 만드는 방법을 배울 수 있어요. [https://www.codecademy.com/tracks/web]


# 템플릿 확장하기
장고의 또 다른 멋진 기능은 템플릿 확장(template extending) 입니다. 무슨 뜻일까요? 바로 여러분의 웹사이트 안의 서로 다른 페이지에서 HTML의 일부를 동일하게 재사용 할 수 있다는 뜻이에요.

post_list.html을 기본으로 하여 base.html을 백본으로 만들고 post_detail.html도 구현합니다.


# 애플리케이션 확장하기

Referral: [https://tutorial.djangogirls.org/ko/extend_your_application/]


# 장고 폼
Referral: [https://tutorial.djangogirls.org/ko/django_forms/]




# 배포하기
Referral: [https://tutorial.djangogirls.org/ko/deploy/]

## Git 저장소 만들기
```
$ git init
Initialized empty Git repository in ~/djangogirls/.git/
$ git config --global user.name "Your Name"
$ git config --global user.email you@example.com
```

git은 이 디렉터리에 모든 파일과 폴더들의 변경 점을 추적할 것인데, 특정 파일들을 무시(ignore)하여 추적하지 않게 할 수 있습니다. 기본 디렉터리에 .gitignore라는 파일을 만들면 됩니다.
```
*.pyc
*~
__pycache__
myvenv
db.sqlite3
/static
.DS_Store
```

## GitHub에 코드 배포하기
```
$ git remote add origin https://github.com/<your-github-username>/my-first-blog.git
$ git push -u origin master
```

## GitHub에서 PythonAnywhere로 코드 가져오기
PythonAnywhere 콘솔에 다음과 같이 입력하세요.
```
$ git clone https://github.com/<your-github-username>/my-first-blog.git
```

PythonAnywhere에 코드 복사본을 올릴 거에요. tree my-first-blog 명령어를 입력해 확인해보세요. 
```
$ tree my-first-blog
my-first-blog/
├── blog
│   ├── __init__.py
│   ├── admin.py
│   ├── migrations
│   │   ├── 0001_initial.py
│   │   └── __init__.py
│   ├── models.py
│   ├── tests.py
│   └── views.py
├── manage.py
└── mysite
    ├── __init__.py
    ├── settings.py
    ├── urls.py
    └── wsgi.py
```

## PythonAnywhere에서: 가상환경(virtualenv) 생성하기 + Django설치 
PythonAnywhere에서도 내 컴퓨터에 있는 것과 같이 작동할 수 있게 가상환경(virtualenv)을 생성할 수 있어요. 
```
$ cd my-first-blog

$ virtualenv --python=python3.6 myvenv
Running virtualenv with interpreter /usr/bin/python3.6
[...]
Installing setuptools, pip...done.

$ source myvenv/bin/activate

(myvenv) $  pip install django~=1.11.0
Collecting django
[...]
Successfully installed django-1.11.3
```

## PythonAnywhere에서 데이터베이스 생성하기
지난번 내 컴퓨터에서 했던 것과 같이 서버에서도 데이터베이스를 초기화 할 거예요. migrate와 createsuperuser를 사용하세요.
```
(mvenv) $ python manage.py migrate
Operations to perform:
[...]
  Applying sessions.0001_initial... OK
(mvenv) $ python manage.py createsuperuser
```

## web app으로 블로그 배포하기
이제 코드는 PythonAnywhere에 있고 우리의 가상환경(virtualenv)도 준비가 되었으며, 정적 파일들도 모여 있고, 데이터베이스도 초기화되었네요. 이제 우리는 웹 앱으로 배포할 준비가 되었어요.

로고를 클릭해 PythonAnywhere 대시보드로 와서 Web을 클릭하고 Add a new web app를 선택하세요.

도메인 이름을 확정한 후, 대화창에 수동설정(manual configuration) ("Django"옵션이 아니에요) 을 클릭하세요. 다음, Python 3.6을 선택하고 다음(Next)을 클릭하면 마법사가 종료됩니다.

- Note "Django"가 아니라 꼭 "수동설정(Manual configuration)"을 선택하세요. 기본 PythonAnywhere Django 설정을 위해서는 이렇게 하는 것이 더 좋아요. ;-)

### 가상환경(virtualenv) 설정하기
PythonAnywhere 설정 화면으로 이동할 거에요. 서버 앱에 변경사항이 있을 때 이 설정 화면으로 들어가야 합니다.

"가상환경(Virtualenv)" 섹션에서 가상환경 경로를 입력해주세요(Enter the path to a virtualenv)라고 쓰여 있는 빨간색 글자를 클릭하고 /home/<your-username>/my-first-blog/myvenv/ 라고 입력합니다. 

### WSGI 파일 설정하기
장고는 "WSGI 프로토콜(WSGI protocol"을 사용해 작동합니다. 이 프로토콜은 파이썬을 이용한 웹사이트를 서비스하기 위한 표준으로 PythonAnywhere에서도 지원합니다. WSGI 설정을 파일을 수정해 우리가 만든 장고 블로그를 PythonAnywhere에서 인식하게 해봅시다.

"WSGI 설정 파일(WSGI configuration file)" 링크(페이지 상단에 있는 "코드(Code)" 섹션 내 /var/www/<your-username>_pythonanywhere_com_wsgi.py부분)를 클릭하면 에디터를 볼 수 있을 것입니다.

모든 내용을 삭제하고 아래 내용을 넣으세요. :
```
import os
import sys

path = '/home/<your-PythonAnywhere-username>/my-first-blog'  # PythonAnywhere 계정으로 바꾸세요.
if path not in sys.path:
    sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.settings'

from django.core.wsgi import get_wsgi_application
from django.contrib.staticfiles.handlers import StaticFilesHandler
application = StaticFilesHandler(get_wsgi_application())
```
이 파일은 PythonAnywhere에게 웹 애플리케이션의 위치와 Django 설정 파일명을 알려주는 역할을 합니다.
StaticFilesHandler는 CSS를 다루기 위한 것입니다.


저장(Save)을 누르고 웹(Web) 탭을 누릅니다.

다 되었어요! 큰 녹색 다시 불러오기(Reload) 버튼을 누르면 여러분은 여러분의 애플리케이션을 볼 수 있을 거예요. 

## 디버깅 팁
만약 Invalid HTTP_HOST header: <your-site-name> . You may need to add <your-site-name> to ALLOWED_HOSTS. 라는 오류메세지가 나온다면 /mysite/settings.py의 마지막 줄에 ALLOWED_HOSTS = ['localhost', '127.0.0.1', '[::1]', '.pythonanywhere.com'] 를 추가 한 뒤에 다시 Web 탭에서 Reload <your-site-name이라는 녹색 버튼을 눌러 주세요.



# 앞으로 해 볼 만한 자료를 추천해주실래요?
Refrral: [https://tutorial.djangogirls.org/ko/whats_next/]

장고걸스: 심화튜토리얼(Django Girls Tutorial: Extensions)을 읽어보세요.

나중에 아래 추천 리스트를 참고해 공부해보세요. 모두 강력 추천하는 자료입니다!

Django's official tutorial
New Coder tutorials
Code Academy Python course
Code Academy HTML & CSS course
Django Carrots tutorial
Learn Python The Hard Way book
Getting Started With Django video lessons
Two Scoops of Django: Best Practices for Django 1.8 book
Hello Web App: Learn How to Build a Web App



